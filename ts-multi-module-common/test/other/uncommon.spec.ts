import {expect} from "chai";
import "mocha";
import {Uncommon} from "../../src";

describe("Uncommon", () => {
    it("says 'world'", () => {
        expect(new Uncommon().world()).to.eq("world");
    });
});
