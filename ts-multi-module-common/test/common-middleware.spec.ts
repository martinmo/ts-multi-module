import * as assert from "assert";
import "mocha";
import {commonMiddleware} from "../src";

describe("Common middleware", () => {

    it("calls next", async () => {
        let nextCalled = false;
        const nextStub: () => Promise<any> = () => {
            nextCalled = true;
            return Promise.resolve(1);
        };

        await commonMiddleware({} as any, nextStub);

        assert.equal(nextCalled, true);
    });

});
