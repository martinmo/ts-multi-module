import {expect} from "chai";
import "mocha";
import {Common} from "../src";

describe("Common", () => {
    it("says 'hello'", () => {
        expect(new Common().hello()).to.eq("hello");
    });

    it("says 'hello2'", () => {
        expect(new Common().hello2()).to.eq("hello2");
    });
});
