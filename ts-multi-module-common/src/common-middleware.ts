import {Context, Middleware} from "koa";

export const commonMiddleware: Middleware = async (ctx: Context, next: () => Promise<any>) => {
    console.log("common middleware request begin");
    await next();
    console.log("common middleware request end");
};
