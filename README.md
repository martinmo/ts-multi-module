Typescript multi module sample project
======================================

This is experimental multi module NPM-Typescript project that tries to provide a similar user experience as Maven provides it for Java based projects.
If this is desirable at all is open to discussion.

Run it
-----
```bash
# setup, test, build, package, it-tests
yarn full-build
```

Goals
-----
* Express dependencies between local none published npm modules
* Download external dependencies
* Build all modules in correct order according to their dependencies
* Support for _compiled_ modules. I.e. modules that need a compile step before they can be used by other modules.
* Provide a _distribution_ that contains all runtime dependencies (npm modules)
* Provide a convenient way to build the whole project. This should include tests and packaging
* Provide a clean way to create releases. Handle version numbers, create Git tags, upload distributions
* All the above should scale. I.e. adding new sub modules should consist of as few steps as possible


Current solution
----------------
The current solution uses [yarn](https://yarn) and [lerna](https://lerna). Lerna provides us with following features:
* Runs npm-scripts in multiple sub modules (ordering the execution according to the dependencies)
* Creates links between sub modules according to their dependencies
* Share dependencies between sub modules in the root node_modules directory
* Lerna reads all the package.json descriptions and uses yarn to download all external dependencies

In order to share dependencies between sub modules there are two possiblities. Use lerna's hoist feature or utilize yarn's workspaces.
As lerna integrates well with yarn and we can use workspaces from lerna.

Sub modules:

* ts-multi-module-common: this is an internal dependency and other modules can depend on it
* ts-multi-module-server: this contains a runnable piece of software. It depends on other sub-modules
* ts-multi-module-docker: build a docker image that contains the bundled application
* ts-multi-module-systemtest: contains systemtests. 
They need a running instance of the software to test and use only public interfaces. 
In our case they test the HTTP-JSON interface, while the application docker images is started
* ts-multi-module-zip: creates a zip archive that contains all nessecary code and dependencies to run ts-multi-module-server

ts-multi-module-docker and ts-multi-module-zip are not part of the lerna/yarn-workspaces construct. 
This is because the need a clean and complete node_modules directory that conains everythings that is needed to run the application and nothing more.

Module conventions
------------------
* If the module creates compiled files (like compiled Typescript code) or other artifacts (e.g. ZIP archive), those files should go into _./dist_ directory
* Common scripts that are called from within the root project are:
** build: compiles code (e.g. tsc for Typescript files)
** test: runs tests


Root module scripts
-------------------
* _clean_ deletes ./dist and ./node_modules directories
* _setup_ runs _clean_ and then installs dependencies from the root project and the sub projects into root'S node_modules
* _build_ runs build script in all sub projects
* _test_ runs test script in all sub projects
* _package_ creates the docker image named _ts-multi-module-$npm_package_version_
* _package-zip_ creates a zip archive named _ts-multi-module-zip-$npm_package_version.tgz_
* _systemtest_ starts the docker image and executes systemtests
* _full-build_ runs _setup, build, test, package, systemtest_


TODO
----
* implement release process


Why not...
---------
* use relative dependencies like "ts-multi-module-common": "../ts-multi-module-common"?


Build
-----
```bash
# full build: clean, fetch dependencies, build, unit test, create docker image, system test, tag docker image
yarn full-build

# without the docker stuff
yarn setup
yarn test
```


run the full build inside docker (mounts your _/var/run/docker.sock_)
```bash
./dev.sh 

# inside you can run
yarn full-build
```
