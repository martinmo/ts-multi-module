#!/usr/bin/env bash

docker build -t ts-mutli-module-development-env ./dev/

docker run -it -v /var/run/docker.sock:/var/run/docker.sock -w /project -v $(pwd):/project --rm ts-mutli-module-development-env
