import {expect} from "chai";
import "mocha";
import {Server} from "../src/server";

describe("Server", () => {
    it("says 'hello world'", () => {
        expect(new Server().hello()).to.eq("hello world");
    });
});
