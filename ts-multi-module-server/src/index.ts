import * as Koa from "koa";
import {commonMiddleware} from "ts-multi-module-common";
import {Server} from "./server";

console.log("starting server ...");

const app = new Koa();

app.use(commonMiddleware);

app.use((ctx, next) => {
    ctx.body = new Server().hello();
});

app.listen(3000);

console.log("started server on http://localhost:3000/");
