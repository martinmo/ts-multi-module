import {Common, Uncommon} from "ts-multi-module-common";

export class Server {

    public hello(): string {
        return new Common().hello() + " " + new Uncommon().world();
    }

}
