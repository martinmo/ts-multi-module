#!/usr/bin/env bash

if [ -f /.dockerenv ]; then
    # "I'm inside matrix";
    echo $(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'  ts-multi-module-systemtest)
else
    echo "localhost";
fi