import * as chai from "chai";
import {expect} from "chai";
import * as chaiHttp from "chai-http";
import "mocha";

chai.use(chaiHttp);

let baseUrl: string;

if (process.env.SERVER) {
    baseUrl = `http://${process.env.SERVER}:3000/`;
} else {
    baseUrl = `http://localhost:3000/`;
}

before("wait for server", async () => {
    console.log("waiting for " + baseUrl + " to be available");
    for (let i = 0; i < 10; i++) {
        try {
            await chai
                .request(baseUrl)
                .get("/");
            console.log("server reachable");
            return;
        } catch (error) {
            console.log("waiting for server...");
            await wait(100);
        }
    }
});

describe("Server", () => {
    it("says 'hello world'", async () => {
        const response = await chai
            .request(baseUrl)
            .get("/");

        expect(response).to.have.status(200);
        expect(response.text).to.eq("hello world");
    });
});

const wait = (millis: number) => new Promise((resolve: any) => setTimeout(resolve, millis));
